FROM  node:latest

COPY src /app
COPY public /app
COPY package-lock.json /app
COPY package.json /app
COPY Makefile /app

WORKDIR /app

RUN ["npm", "install"]
RUN ["npm", "start"]

EXPOSE 5000
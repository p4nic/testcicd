
import './App.css';

function App() {
  return (
    <>
  <div id="body">
    <div className="other">
      <div className="centered">
        <h2>Hi, I'm Andrew Karpisz!</h2>
        <br />
        <img alt=""src="./public/assets/1606257456407.jpeg" id="pic" />
        <br />
        <br />
      </div>
      <div className="topother">
        <a href="https://github.com/andrewakcybersec"> - Cybersecurity Analyst / Cloud Security Architect /
          Application Security Engineer
          <br />
          <br /> - and Other Titles That Could Potentially Fit My Skillset</a>
        <br />
        <br />
        <a href="https://www.credly.com/badges/b37552ef-087a-448c-96ca-0bb4a457ceb3/public_url"> - CompTIA Cybersecurity
          Professional Certifications</a>
          <br />
          <br />
          <div id="stacked">
          <a href="https://www.credly.com/badges/b37552ef-087a-448c-96ca-0bb4a457ceb3/public_url"><img alt="" src="./public/assets/comptia-network-vulnerability-assessment-professional-cnvp-stackable-certification.png"
          id="lastbadge" /></a>
        </div>
        </div>
        
        <br />
        <div id="badges">
        <a href="https://www.credly.com/badges/67252bfc-e290-4ed2-8509-7e7363a0fabb/public_url"><img alt="" src="./public/assets/SecurityPlus Logo Certified CE.jpg" className="comptia" /></a>
        <a href="https://www.credly.com/badges/78df8005-4284-4406-9b27-7afc035ef8e1/public_url"><img alt="" src="./public/assets/PenTest+ce certified Logo BLACK.jpg" className="comptia" /></a>
      </div>
      </div>
    <div className="other">
      <h2>👨‍💻 Software Development Skills Certifications</h2>
      <br/> - <b>4+ Years</b> Enterprise Development Experience
      <br />- <b>Python</b>
      - <a href="https://www.hackerrank.com/certificates/iframe/68be2a48a4c5">Python Hackerrank Cert</a>
      <br />- <b>Go</b>
      - <a href="https://www.hackerrank.com/certificates/20ecb122f4ae">Go Hackerrank Cert</a>
      <br />- <b>JavaScript</b>
      - <a href="https://www.hackerrank.com/certificates/3232d02ee424">JavaScript Hackerrank Cert</a>
      <br />- <b>Problem Solving</b>
      - <a href="https://www.hackerrank.com/certificates/9371aa42bb8b">Problem Solving Hackerrank Cert</a>
      <br />
    </div>
    <br />
    <br />
    <div className="other">
      - <b>Other skills</b>: <br />
      - Linux administration and BASH scripting<br />
      - Application Security Testing<br />
      - Network Penetration
      - MySQL and PostgreSQL<br />
      - MongoDB<br />
      - ReactJS<br />
      - Basic proficiency in Java<br />
      - C, C++, GDB proficiency<br />
      - Docker/container/Kubernetes experience in public and private cloud environments<br />
    </div>
  </div>
  </>
  );
}

export default App;

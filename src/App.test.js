import { render, screen } from '@testing-library/react';
import App from './App';

test('renders profile', () => {
  render(<App />);
  const linkElement = screen.getByText(/Andrew Karpisz/i);
  expect(linkElement).toBeInTheDocument();
});
